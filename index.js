
let loggers = require('./configWinston');

/**
 * Config : Set the loggers.
 * @param {Object} winstonLoggers : Path to the config file. In this file, the winston.loggers need to be exported.
 */
function configure(winstonLoggers)
{
  try
  {
    loggers = winstonLoggers;
  }
  catch (e)
  {
    throw e;
  }
}

/**
 * Production
 * @param {string} level
 * @param {string} message
 */
function production(level, message)
{
  if (loggers)
    loggers.get('production').log({level: level, message: message});
}

/**
 * Dev
 * @param {string} level
 * @param {string} message
 */

function dev(level, message)
{
  if (loggers)
    loggers.get('dev').log({level: level, message: message});
}

/**
 * Debug
 * @param {string} level
 * @param {string} message
 */

function debug(level, message)
{
  if (loggers)
    loggers.get('debug').log({level: level, message: message});
}

/**
 * Log
 * @param {string} level
 * @param {string|Object} message
 * @param {string} origin
 */
function log(level, message, origin = "")
{
  // Stringify
  if (typeof message !== "string")
    message = JSON.stringify(message);

  if (origin !== "")
    message = origin + " : "+message;

  switch (process.env.NODE_ENV)
  {
    case 'production':
    {
      return production(level, message);
    }

    case 'dev':
    {
      return dev(level, message);
    }

    case 'debug':
    {
      return debug(level, message);
    }

    default:
    {
      return dev(level, message);
    }
  }
}

/**
 * Stream
 * @type {{write: stream.write}}
 */
const stream =
{
  write: (message, encoding) =>
  {
    log('info', message);
  }
};

/**
 * Track
 * @param {string} module_
 * @param {string} arguments_
 * @return {string}
 */
function track(module_, arguments_)
{
  return "[ "+module_.id+" - "+arguments_.callee.name+" ]";
}




module.exports =
{
  log,
  configure,
  track,
  stream
};