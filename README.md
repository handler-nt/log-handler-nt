# LogHandler

Module for logging in a NodeJS project. It is based on Winston (https://github.com/winstonjs/winston).


##Installation
<code>$ yarn install log-handler-nt --save</code>

##Usage


###Configuration [optionnal]

You can set your own winston loggers with the function <code>configure(winstonLoggers)</code><br>
See https://github.com/winstonjs/winston#working-with-multiple-loggers-in-winston for more details.<br>

The configuration is optionnal, the module is using a default configuration.

configWinston.js

````javascript
const winston = require('winston');
const appRootPath = require('app-root-path');


const options =
{
  fileProd:
  {
    level: 'info',
    filename: appRootPath+'/logs/app.log',
    format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
    maxsize: 5242880, // 5MB
    maxFiles: 5
  },

  consoleVerbose:
  {
    level: 'verbose',
    handleExceptions: true,
    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
    colorize: true
  },

  consoleDebug:
  {
    level: 'debug',
    handleExceptions: true,
    format: winston.format.combine(winston.format.colorize(), winston.format.simple()),
    colorize: true
  }
};

// PROD LOGGER
winston.loggers.add('production',
{
  transports: [new winston.transports.File(options.fileProd)],
  exitOnError: false
});

// DEV LOGGER
winston.loggers.add('dev',
{
  transports: [new winston.transports.Console(options.consoleVerbose)],
  exitOnError: false
});

// DEBUG LOGGER
winston.loggers.add('debug',
{
  transports: [new winston.transports.Console(options.consoleDebug)],
  exitOnError: false
});



module.exports = winston.loggers;
````

###Documentation

- <code>log(level, message, origin = "")</code><br>
	@param level {string} level logging with priority
	
	````json5
	{ 
    error: 0, 
    warn: 1, 
    info: 2, 
    verbose: 3, 
    debug: 4, 
    silly: 5 
  }
  ````
  @param message {string|object} : Message to log<br>
  @param origin {string} : Optionnal 
  
- <code>stream.write</code><br>
	Enable logging using stream.
	
	
- <code>track(module_, arguments_)</code>
	@param module_ {id} : ID of the module
	@param arguments_ {callee: {name}}